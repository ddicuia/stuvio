var CrossHatchShader = function(callback) {

    var chfragment;
    var chvertex;
    var material;


    $.get('shaders/chfragment.glsl', function(data) {
        chfragment = data;
        $.get('shaders/chvertex.glsl', function(data) {
            chvertex = data;
            init();
        });
    });

    function init() {
        var id = "assets/shades/hatch_";

        var material = new THREE.ShaderMaterial({

            uniforms: {
                showOutline: { 
                    type: 'f',
                    value: 0
                },
                ambientWeight: { 
                    type: 'f',
                    value: 0
                },
                diffuseWeight: { 
                    type: 'f',
                    value: 0
                },
                rimWeight: { 
                    type: 'f',
                    value: 1
                },
                specularWeight: { 
                    type: 'f',
                    value: 1
                },
                shininess: { 
                    type: 'f',
                    value: 1
                },
                invertRim: { 
                    type: 'i',
                    value: 0
                },
                inkColor: {
                    type: 'v4',
                    value: new THREE.Vector3(0, 0, 0)
                },
                solidRender: { 
                    type: 'i',
                    value: 0
                },
                resolution: { 
                    type: 'v2',
                    value: new THREE.Vector2(0, 0)
                },
                bkgResolution: { 
                    type: 'v2',
                    value: new THREE.Vector2(0, 0)
                },
                lightPosition: { 
                    type: 'v3',
                    value: new THREE.Vector3(-10, 100, 0)
                },
                hatch1: {
                    type: 't',
                    value: THREE.ImageUtils.loadTexture(id + '0.jpg')
                },
                hatch2: {
                    type: 't',
                    value: THREE.ImageUtils.loadTexture(id + '1.jpg')
                },
                hatch3: {
                    type: 't',
                    value: THREE.ImageUtils.loadTexture(id + '2.jpg')
                },
                hatch4: {
                    type: 't',
                    value: THREE.ImageUtils.loadTexture(id + '3.jpg')
                },
                hatch5: {
                    type: 't',
                    value: THREE.ImageUtils.loadTexture(id + '4.jpg')
                },
                hatch6: {
                    type: 't',
                    value: THREE.ImageUtils.loadTexture(id + '5.jpg')
                },

                repeat: {
                    type: 'v2',
                    value: new THREE.Vector2(0, 0)
                }
            },
            vertexShader: chvertex,
            fragmentShader: chfragment

        });

        material.uniforms.repeat.value.set(1, 1);
        material.uniforms.hatch1.value.wrapS = material.uniforms.hatch1.value.wrapT = THREE.RepeatWrapping;
        material.uniforms.hatch2.value.wrapS = material.uniforms.hatch2.value.wrapT = THREE.RepeatWrapping;
        material.uniforms.hatch3.value.wrapS = material.uniforms.hatch3.value.wrapT = THREE.RepeatWrapping;
        material.uniforms.hatch4.value.wrapS = material.uniforms.hatch4.value.wrapT = THREE.RepeatWrapping;
        material.uniforms.hatch5.value.wrapS = material.uniforms.hatch5.value.wrapT = THREE.RepeatWrapping;
        material.uniforms.hatch6.value.wrapS = material.uniforms.hatch6.value.wrapT = THREE.RepeatWrapping;


        var settings = {
            ambient: 8,
            diffuse: 100,
            specular: 100,
            rim: 46,
            shininess: 49,
            invertRim: false,
            displayOutline: false,
            solidRender: false,
            model: 3,
            paper: 0,
            inkColor: [72, 72, 164]
        };

        material.uniforms.inkColor.value.set(settings.inkColor[0] / 255, settings.inkColor[1] / 255, settings.inkColor[2] / 255, 1);
        material.uniforms.ambientWeight.value = settings.ambient / 100;
        material.uniforms.diffuseWeight.value = settings.diffuse / 100;
        material.uniforms.rimWeight.value = settings.rim / 100;
        material.uniforms.specularWeight.value = settings.specular / 100;
        material.uniforms.shininess.value = settings.shininess;
        material.uniforms.invertRim.value = settings.invertRim ? 1 : 0;
        material.uniforms.solidRender.value = settings.solidRender ? 1 : 0;
        material.uniforms.inkColor.value.set(settings.inkColor[0] / 255, settings.inkColor[1] / 255, settings.inkColor[2] / 255, 1);

        material.depthWrite = true;
        material.uniforms.showOutline.value = 0;

        callback(material);
    }

};