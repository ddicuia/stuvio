var LayoutManager = (function() {

    var DECREASE = 0.25;




    return {

        go: function(options) {

            var points = [];
            var width = options.width;
            var height = options.height;
            var startX = options.startX;
            var startY = options.startY;
            var radiusMax = options.radiusMax;
            var radiusMin = options.radiusMin;
            var callback = options.onComplete;
            var totalTries = 0;
            var allowedOverlap = options.allowedOverlap || 1;
            var maxPoints = options.maxPoints || 20;

            position();

            function position() {
                if (points.length >= maxPoints) return;

                var tries = 0;
                var positioned = false;
                var p = {
                    r: radiusMax,
                    x: startX,
                    y: startY
                }
                positioned = !isOverlapping(p) && isWithin(p);
                if (!positioned) {
                    totalTries++;

                    if (totalTries < 6000 && radiusMax > radiusMin) {

                        startX = stuvio.random.float(width);
                        startY = stuvio.random.float(height);

                        radiusMax -= DECREASE;
                        position();

                    } else {
                        callback(points);
                    }


                } else {
                    points.push(p);

                    position();
                }

                function isWithin(p) {
                    return p.x + p.r < width && p.x - p.r > 0 &&
                        p.y + p.r < height && p.y - p.r > 0;
                }

                function isOverlapping(p) {
                    var pp;
                    for (var i = 0; i < points.length; i++) {
                        pp = points[i];

                        if (p != pp) {
                            var dx = pp.x - p.x;
                            var dy = pp.y - p.y;
                            var r = pp.r + p.r;
                            if (dx * dx + dy * dy <= r * r * allowedOverlap) {
                                return true;
                            }
                        }
                    }

                    return false;
                }

            }

        }

    }


})();