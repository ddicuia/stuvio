THREE.ShaderTypes = {

    'noisyDiffuse': {

        uniforms: {

            uDirLightPos: {
                type: "v3",
                value: new THREE.Vector3(0.0, 1.0, 0.0)
            },


            color: {
                type: "v3",
                value: new THREE.Vector3(0.0, 0.0, 1.0)
            },

            uKd: {
                type: "f",
                value: 1.0
            }
        },

        vertexShader: [

            "varying vec3 vNormal;",

            "void main() {",

            "   gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
            "   vNormal = normalize( normalMatrix * normal );",

            "}"

        ].join("\n"),

        fragmentShader: [


            "uniform vec3 uDirLightPos;",
            "uniform float uKd;",
            "uniform vec3 color;",

            "varying vec3 vNormal;",

            "float nrand( vec3 n )",
            "{",
            "  return fract(sin(dot(n.xy, vec2(12.9898, 78.233)))* 43758.5453);",
            "}",

            "vec3 lin2srgb( vec3 cl )",
            "{",
            "   vec3 c_lo = 12.92 * cl;",
            "   vec3 c_hi = 1.055 * pow(cl,vec3(0.41666)) - 0.055;",
            "   vec3 s = step( vec3(0.0031308), cl);",
            "   return mix( c_lo, c_hi, s );",
            "}",

            // see http://www.opengl.org/registry/specs/ARB/framebuffer_sRGB.txt
            "vec3 srgb2lin( vec3 cs )",
            "{",
            "    vec3 c_lo = cs / 12.92;",
            "    vec3 c_hi = pow( (cs + 0.055) / 1.055, vec3(2.4) );",
            "    vec3 s = step(vec3(0.04045), cs);",
            "    return mix( c_lo, c_hi, s );",
            "}",

            "vec3 trunc( vec3 a, float l )",
            "{",
            "   return floor(a*l)/l;",
            "}",

            "void main() {",

            "vec3 lVector = normalize( uDirLightPos.xyz );",

            "vec3 normal = normalize( vNormal );",
            "float nrnd = nrand( normal );",
            // diffuse: N * L. Normal must be normalized, since it's interpolated.
            "float diffuse = dot( normal+nrnd, lVector );",

            "const float levels = 8.0;",
            "if (diffuse > uKd) { diffuse = 1.0; }",
            // "vec3 val_noise = vec3(diffuse  + nrnd/levels);",
            "vec3 val_noise_srgb = srgb2lin(lin2srgb(vec3(diffuse)) + nrnd/levels);",

            // "vec3 t = vec3(1.0,1.0,1.0) * diffuse * uKd;",
            // "vec3 t = trunc( lin2srgb(val_noise), levels );",
            "vec3 t = trunc( lin2srgb(val_noise_srgb), levels );",
            "gl_FragColor = vec4( t*color, 1.0 );",

            "}"

        ].join("\n")

    },

    'gradient': {

        uniforms: {
            texture: {
                type: 't',
                value: null
            },
            gradientColor: {
                type: "v3",
                value: new THREE.Vector3()
            },
        },

        vertexShader: [
            "varying vec2 vUv;",
            "void main() { ",
            "   vUv = uv;",
            "   gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);",
            "}",
        ].join("\n"),

        fragmentShader: [

            "varying vec2 vUv;",
            "uniform sampler2D texture;",
            "uniform vec3 gradientColor;",
            "void main() {",
            "    vec4 col = texture2D(texture, vUv);",
            "    if(col.a <= 0.9) {",
            "       discard;",
            "    } else {",
            "       gl_FragColor = vec4(gradientColor * vUv.y,0.0);",
            "    }",
            "}"

        ].join("\n")
    },


    'flatNoise': {

        uniforms: {
            texture: {
                type: 't',
                value: null
            },
            gradientColor: {
                type: "v3",
                value: new THREE.Vector3(0.0, 1.0, 0.0)
            },
        },

        vertexShader: [
            "varying vec2 myposition;",
            "uniform vec3 gradientColor;",

            "void main() { ",
            "   gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);",
            "   myposition = uv.xy;",
            "}",
        ].join("\n"),

        fragmentShader: [

            "varying vec2 myposition;",
            "uniform vec3 gradientColor;",

            "float nrand( vec2 n )",
            "{",
            "  return fract(sin(dot(n.xy, vec2(12.9898, 78.233)))* 43758.5453);",
            "}",

            "void main() {",
            "       vec3 t = myposition.yyy + nrand(myposition);",
            "       gl_FragColor = vec4(t * gradientColor, 0.0);",
            "}"

        ].join("\n")
    }

}