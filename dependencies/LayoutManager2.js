var LayoutManager = (function() {

    var DECREASE = 1;

    var points;
    var width;
    var height;
    var startX;
    var startY;
    var radiusMax;
    var radiusMin;
    var points;
    var callback;
    var totalTries;
    var maxPoints;
    var squaredDistance;

    var FRAMEMARGIN = {
        x: 10,
        y: 10
    }



    return {

        go: function(options) {
            points = [];
            maxPoints = options.maxPoints || Infinity;
            width = options.width;
            height = options.height;
            startX = options.startX;
            startY = options.startY;
            radiusMax = options.radiusMax;
            radiusMin = options.radiusMin;
            callback = options.onComplete;
            totalTries = 0;

            position();

            function position() {
                var tries = 0;
                var positioned = false;
                var p = {
                    r: radiusMax,
                    x: startX,
                    y: startY
                }

                positioned = !isOverlapping(p) && isWithin(p);

                if (!positioned) {
                    totalTries++;

                    if (totalTries < 4000 && radiusMax > radiusMin && points.length < maxPoints) {

                        startX = stuvio.random.float(width);
                        startY = stuvio.random.float(height);

                        radiusMax -= DECREASE;
                        position();

                    } else {
                        callback(points);
                    }


                } else {
                    points.push(p);
                    position();
                }

                function isWithin(p) {
                    return p.x + p.r < width - FRAMEMARGIN.x && p.x - p.r > FRAMEMARGIN.x &&
                        p.y + p.r < height - FRAMEMARGIN.y && p.y - p.r > FRAMEMARGIN.y;
                }

                function isOverlapping(p) {
                    var pp;
                    for (var i = 0; i < points.length; i++) {
                        pp = points[i];

                        if (p != pp) {
                            var dx = pp.x - p.x;
                            var dy = pp.y - p.y;
                            var r = pp.r + p.r;
                            if (dx * dx + dy * dy <= r * r) {
                                return true;
                            }
                        }
                    }
                    return false;
                }

            }






        }



    }


})();