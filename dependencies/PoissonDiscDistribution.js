//based on http://www.cs.ubc.ca/~rbridson/docs/bridson-siggraph07-poissondisk.pdf

var PoissonDiscDistribution = (function(o) {

    var settings = {

        tries: o.tries || 7000,

        startRadius: o.startRadius || 50,

        start: {
            x: o.start.x,
            y: o.start.y
        },

        width: o.width,
        height: o.height,
        radiusDistance: 100
    }

    var width = settings.width;
    var height = settings.height;
    var cellSize = settings.startRadius * 2 * Math.SQRT1_2;



    var gridWidth = Math.ceil(width / cellSize);
    var gridHeight = Math.ceil(height / cellSize);
    var grid = new Array(gridWidth * gridHeight);
    var points = [];
    var queue = [];
    var currentRadius = settings.startRadius;

    var startPoint = {
        x: settings.start.x,
        y: settings.start.y,
        radius: currentRadius
    }

    points.push(startPoint);
    queue.push(startPoint);

    return {

        points: points,

        next: function() {

            var found = false;
            var n = queue.length;
            if (!n) return;

            while (n > 0 && !found) {

                for (var i = 0; i < settings.tries; i++) {

                    var index = Math.round(Math.random() * (queue.length - 1));
                    var p = queue[index];
                    var q = generateAround(p);

                    if (withinBorders(q) && !isOverlapping(q)) {
                        queue.push(q);
                        points.push(q);
                        grid[gridWidth * (q.y / cellSize | 0) + (q.x / cellSize | 0)] = q;
                        found = true;
                        console.log("found!")
                        break;
                    } else {
                        if (currentRadius > 20) currentRadius -= .1;
                    }
                }

                if (i === settings.tries) {
                    queue[index] = queue[--n];
                    queue.pop();
                }

            }

            if (n === 0) console.log("exiting");

        }

    }

    function isOverlapping(p) {

        // neigbor distance
        // var n = 2;
        // // cell coordinates
        // var x = p.x / cellSize | 0;
        // var y = p.y / cellSize | 0;
        // // search
        // var x0 = Math.max(x - n, 0);
        // var y0 = Math.max(y - n, 0);
        // var x1 = Math.min(x + n + 1, gridWidth);
        // var y1 = Math.min(y + n + 1, gridHeight);

        // for (var y = y0; y < y1; ++y) {
        //     var o = y * gridWidth;
        //     for (var x = x0; x < x1; ++x) {
        //         var g = grid[o + x];
        //         if (g) {
        //             var rd = g.radius * g.radius +  p.radius * p.radius;
        //             if (distance2(g, p) < rd ) return true;
        //         }
        //     }
        // }
        // 
        for (var i = 0; i < points.length; i++) {
            var g = points[i]
            var rd = g.radius * g.radius + p.radius * p.radius;
            if (distance2(g, p) < rd * 2) return true;
        }

        return false;
    }

    function withinBorders(p) {
        return p.x + p.radius < settings.width && p.x - p.radius >= 0 && p.y + p.radius < settings.height && p.y - p.radius >= 0;
    }

    function distance2(a, b) {
        var dx = b.x - a.x;
        var dy = b.y - a.y;
        return dx * dx + dy * dy;
    }


    function generateAround(p) {

        // random point in annulus 
        // http://stackoverflow.com/a/9048443/64009
        var r = settings.radiusDistance;
        var currentRadiusSq = currentRadius * currentRadius;
        var A = 4 * r * r - currentRadiusSq;
        var θ = Math.random() * 2 * Math.PI;
        r = Math.sqrt(Math.random() * A + currentRadiusSq);
        return {
            x: p.x + r * Math.cos(θ),
            y: p.y + r * Math.sin(θ),
            radius: currentRadius
        }
    }


})