var Generator = (function() {

    var canvas = document.createElement('canvas')
    var context = canvas.getContext('2d')
    var MINRADIUS = 50;
    var width = 520;
    var height = 770;
    //margin between objects squared

    var points;

    var settings = {



        centered: {
            type: 'boolean',
            label: 'Centered layout',
            value: true
        },
        maxRadius: {
            type: 'number',
            label: 'maxRadius',
            range: [0, 200],
            value: 1,
            step: 1
        },



    }

    return {

        context: context,

        settings: settings,

        initialize: function(done) {
            done();
        },

        generate: function(done) {

            context.clearRect(0, 0, canvas.width, canvas.height)

            LayoutManager.go({
                radiusMin: (MINRADIUS),
                radiusMax: (settings.maxRadius.value + MINRADIUS),
                width: width,
                height: height,
                startX: settings.centered.value ? width * .5 : stuvio.random.float(width),
                startY: settings.centered.value ? height * .5 : stuvio.random.float(height),
                onComplete: this.ok.bind(this)
            });

            done()

        },

        ok: function(points) {

            var ratio = canvas.width / width;


            for (var i = 0; i < points.length; i++) {
                var p = points[i];
                context.beginPath();
                context.fillStyle = "hsl(" + (stuvio.random.int(360)) + ", 72%, 63%)";
                context.arc(p.x * ratio, p.y * ratio, p.r * ratio, 0, 2 * Math.PI, false);
                context.fill();
            };
        },


        destroy: function(done) {

            done()
        }
    }

})();