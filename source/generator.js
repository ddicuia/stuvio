/*

    This is an example of a WebGL based generator, built on top of the Three.js library. It shows 
    how to use a rendering context created by a third party library, as well as how to utilize the 
    `studio.env` attribute in order to perform conditional actions based on whether the generator 
    is running on the web, or being used to generate high resolution output for print.

    When the generator is being used to create print ready images (this happens on our servers once 
    a customers has created and purchased a print), the `studio.env` attribute is set to ’print’ as 
    opposed to ‘web’. For WebGL based generators, the drawing buffer must be preserved when in this 
    mode, and so when constructing the WebGL context (in this case via the Three.js WebGLRenderer 
    Class) you must ensure that `preserveDrawingBuffer` is set to `true`

*/



var Generator = (function() {

    var IMAGEMARGINS = 50;
    var SHAPEMARGINS = 0;
    var UKD = 0.3;
    var MINRADIUS = 30;
    var DEBUG = false;
    var CAMERAFAR = 6000;
    var CAMERANEAR = 0.1;
    var MINSHAPESIZE = 100;
    var MINIMGWDTIH = 100;
    var MAXIMGWDTIH = 500;
    var WIDTH = 520;
    var HEIGHT = 770;
    var BGWIDTH = Math.round(WIDTH * 1.25);
    var BGHEIGHT = Math.round(HEIGHT * 1.25);
    var backgroundPlane;
    var color;

    var phongObjectsUrl = [
        "solid9.obj",
        "solid8.obj",
        "solid4.obj",
        "solid3.obj",
        "solid2.obj",
        "solid11.obj",
        "solid10.obj",
        "solid1.obj",
        "polystick2.obj",
        "polystick1.obj",
        "polysphere.obj",
        "polygonsolid3.obj",
        "polygonsolid2.obj",
        "loop3.obj",
        "polygonsolid1.obj",
        "plane3.obj",
        "plane2.obj",
        "plane1.obj",
        "plane6.obj",
        "plane4.obj",
        "plane5.obj",
        "column4.obj",
        "column3.obj",
        "column2.obj"
    ];
    var otherObjectsUrl = [
        "solid13.obj",
        "solid12.obj",
        "pipe5.obj",
        "pipe4.obj",
        "pipe3.obj",
        "pipe1.obj",
        "pipe2.obj",
        "smoothsolid5.obj",
        "smoothsolid4.obj",
        "smoothsolid3.obj",
        "ellipsoid3.obj",
        "ellipsoid2.obj",
        "ellipsoid1.obj",
        "sphere.obj",
        "stick2.obj",
        "stick1.obj",
        "sausage7.obj",
        "sausage6.obj",
        "sausage5.obj",
        "sausage4.obj",
        "sausage3.obj",
        "sausage2.obj",
        "sausage1.obj",
        "loop2.obj",
        "loop1.obj",
        "smoothsolid2.obj",
        "trunc_cone4.obj",
        "trunc_cone3.obj",
        "trunc_cone2.obj",
        "trunc_cone1.obj",
        "solid7.obj",
        "smoothsolid1.obj",
        "rod2short.obj",
        "rod2mid.obj",
        "rod2.obj",
        "rod1short.obj",
        "rod1.obj",
        "solid5.obj",
        "solid6.obj",
        "column1.obj"
    ]


    var renderer;
    var manager;
    var camera;
    var scene;
    var light;

    var width;
    var height;

    var noisyMaterial;
    var shinyMaterial;
    var matteMaterial;
    var frameMaterial;
    var flatNoiseMaterial;

    var frame;
    var objects = [];
    var phongObjects = [];
    var otherObjects = [];
    var brushes = [];
    var cutouts = [];

    var settings = {





        frameType: {
            type: 'number',
            label: 'Frame Type',
            description: 'Select the type of frame to display',
            range: [0, 6],
            value: 3,
            step: 1
        },

        lightHue: {
            type: 'number',
            label: 'light color',
            description: 'Adjust the colour of the objects',
            hidden: true,
            range: [0, 360],
            value: 50,
            step: 1
        },

        bgHue: {
            type: 'number',
            label: 'Background color',
            description: 'Adjust the color of the Background',
            range: [0, 360],
            value: 50,
            step: 1

        },



        bgVisible: {
            type: 'boolean',
            hidden: true,
            value: true
        },


        maxShapeSize: {
            type: 'number',

            label: 'Max. Shape Size',
            hidden: true,
            description: 'Adjust maximum size of 3D shape elements',
            range: [10, 600],
            value: 100,
            step: 1
        },




        maxImageSize: {
            type: 'number',
            label: 'maxImageSize',
            hidden: true,
            range: [MINIMGWDTIH, 800],
            value: 100,
            step: 1
        },


        lightDirection: {
            type: 'number',
            label: 'Light Direction',
            description: 'Adjust lighting direction',
            range: [0, 360],
            value: 0,
            step: 0.01
        },



        offsetRotation: {
            type: 'number',
            label: 'Object Rotation',
            description: 'Adjust rotation of 3D objects',
            range: [0, 360],
            value: 0,
            step: 1
        },

        saturation: {
            type: 'number',
            label: 'saturation',
            description: 'Change colours saturation',
            range: [0, 1],
            // hidden: true,
            value: .5,
            step: 0.001
        },




    }

    var o = {

        context: null,

        settings: settings,

        initialize: function(done) {
            frameGeometries = [];
            objectGeometries = [];
            brushes = [];
            cutouts = [];

            manager = new THREE.LoadingManager();


            noisyMaterial = this.createShaderMaterial("noisyDiffuse");

            shinyMaterial = new THREE.MeshPhongMaterial({
                shininess: 100
            });

            matteMaterial = new THREE.MeshPhongMaterial({
                shininess: 0
            });

            shinyMaterial.side = THREE.DoubleSide;
            matteMaterial.side = THREE.DoubleSide;


            frameMaterial = new THREE.MeshLambertMaterial({
                color: 0xdddddd
            });


            flatNoiseMaterial = this.createShaderMaterial("flatNoise");



            manager.onLoad = function(item, loaded, total) {

                // sort arrays alphabetically 
                objectGeometries.sort(alphaSort);
                frameGeometries.sort(alphaSort);
                brushes.sort(alphaSort);
                cutouts.sort(alphaSort);

                function alphaSort(a, b) {
                    if (a.name < b.name) return 1;
                    if (a.name > b.name) return -1;
                    return 0;
                }

                this.initThree(done);


            }.bind(this);

            this.loadFrames();
            this.loadImages();
            this.loadShapes();




        },




        generate: function(done) {

            width = this.context.canvas.width;
            height = this.context.canvas.height;

            //delete all children
            while (objects.length) {
                scene.remove(objects.pop());
            }

            // update light and materials
            var theta = settings.lightDirection.value / 180 * Math.PI - Math.PI;
            light.position.x = Math.cos(theta);
            light.position.y = Math.sin(theta);
            light.position.z = 0.5;

            var randomSaturation = settings.saturation.value;
            color = this.hslToRgb(settings.lightHue.value / 360, randomSaturation, .5);
            light.color.setRGB(color.x, color.y, color.z);

            // update shaders
            noisyMaterial.uniforms.color.value = color;
            noisyMaterial.uniforms.uDirLightPos.value = light.position;
            noisyMaterial.uniforms.uKd.value = UKD;


            if (settings.bgVisible.value) {

                //adapt background
                var newZ = -600;
                var newWidth = BGWIDTH * -((newZ - camera.position.z) / camera.position.z);
                var newScale = (newWidth / BGWIDTH)
                backgroundPlane.scale.x = newScale;
                backgroundPlane.scale.y = newScale;
                backgroundPlane.position.z = newZ;

                scene.add(backgroundPlane);
                objects.push(backgroundPlane);

            }


            this.addFrame();
            this.addImageLayer();
            this.addShapeLayer();

            camera.aspect = width / height
            camera.updateProjectionMatrix()
            var c = this.hslToRgb(settings.bgHue.value / 360, randomSaturation, .5);
            flatNoiseMaterial.uniforms.gradientColor.value = c;
            renderer.setClearColor(new THREE.Color(c.x, c.y, c.z));
            renderer.setViewport(0, 0, width, height);

            renderer.render(scene, camera)

            done();

        },

        addImageLayer: function(z) {


            var margins = IMAGEMARGINS;
            var options = {

                allowedOverlap: 1,
                radiusMin: MINIMGWDTIH,
                radiusMax: settings.maxImageSize.value + MINRADIUS,
                width: WIDTH - margins * 2,
                height: HEIGHT - margins * 2,
                startX: stuvio.random.int(WIDTH),
                startY: stuvio.random.int(HEIGHT),

                onComplete: this.positionImages.bind(this)
            }

            LayoutManager.go(options);
        },


        addFrame: function() {
            var frameIndex = settings.frameType.value;
            if (frameIndex == frameGeometries.length) return;

            frame = frameGeometries[frameIndex];
            frame.material = frameMaterial;


            var cameraDistance = camera.position.z;

            var newZ = -400;
            var size = 1000 * -((newZ - cameraDistance) / cameraDistance);


            frame.geometry.computeBoundingBox()
            frame.scale.x = size;
            frame.scale.y = size;
            frame.scale.z = size;

            frame.position.z = newZ;

            scene.add(frame)
            objects.push(frame);


        },


        addShapeLayer: function(z) {

            var options = {};
            options.radiusMin = MINSHAPESIZE;
            options.radiusMax = MINSHAPESIZE + settings.maxShapeSize.value;
            options.width = WIDTH - SHAPEMARGINS * 2;
            options.height = HEIGHT - SHAPEMARGINS * 2;
            options.allowedOverlap = .4;
            options.startX = stuvio.random.float(WIDTH);
            options.startY = stuvio.random.float(HEIGHT);
            options.onComplete = this.positionShapes.bind(this);

            LayoutManager.go(options);

        },

        positionShapes: function(points) {

            var margins = SHAPEMARGINS;

            for (var i = 0; i < points.length; i++) {

                var p = points[i];
                var pos = this.get3dPoint(p.x + margins, p.y + margins);
                var object;

                if (stuvio.random.bool()) {

                    object = stuvio.random.item(phongObjects);
                    object.material = stuvio.random.bool() ? shinyMaterial : matteMaterial;

                } else {

                    object = stuvio.random.item(otherObjects);
                    object.material = stuvio.random.bool() ? shinyMaterial : noisyMaterial;

                }


                if (DEBUG) {

                    var debugSphere = new THREE.Mesh(new THREE.SphereGeometry(p.r), new THREE.MeshNormalMaterial({
                        wireframe: true
                    }));
                    debugSphere.position.x = pos.x;
                    debugSphere.position.y = pos.y;

                    scene.add(debugSphere);
                    objects.push(debugSphere);
                }

                object.scale.x = p.r * 2;
                object.scale.y = p.r * 2;
                object.scale.z = p.r * 2;


                var offsetRotation = settings.offsetRotation.value / 180 * Math.PI;

                object.rotation.x = stuvio.random.float(0, 2 * Math.PI) + offsetRotation;
                object.rotation.z = stuvio.random.float(0, 2 * Math.PI) + offsetRotation;
                object.rotation.y = stuvio.random.float(0, 2 * Math.PI) + offsetRotation;
                object.position.x = pos.x;
                object.position.y = pos.y;
                // object.position.z = stuvio.random.int(settings.z.value) * stuvio.random.sign();

                scene.add(object);
                objects.push(object);


            };
        },

        positionImages: function(points) {

            var cameraDistance = camera.position.z;
            var newZ = stuvio.random.sign() * 200;
            var margins = IMAGEMARGINS;


            for (var i = 0; i < points.length; i++) {

                var p = points[i];

                var pos = this.get3dPoint(p.x + margins, p.y + margins);
                var planeMaterial;
                var texture;



                if (stuvio.random.bool()) {
                    texture = stuvio.random.item(cutouts);

                    planeMaterial = this.createShaderMaterial("gradient", {
                        texture: {
                            type: 't',
                            value: texture
                        },
                        gradientColor: {
                            type: 'v3',
                            value: color
                        }
                    });

                    texture.needsUpdate = true;

                } else {

                    texture = stuvio.random.item(brushes);
                    planeMaterial = new THREE.MeshBasicMaterial({
                        map: texture,
                        transparent: true
                        // side: THREE.DoubleSide
                    });
                    texture.needsUpdate = true;

                }


                // enlarge size as images are pushed back
                var size = (p.r * 2) * -((newZ - cameraDistance) / cameraDistance);
                var planeGeometry = new THREE.PlaneBufferGeometry(size, size);
                var object = new THREE.Mesh(planeGeometry, planeMaterial);

                // if (DEBUG) {

                //     var debugSphere = new THREE.Mesh(new THREE.SphereGeometry(size / 2), new THREE.MeshNormalMaterial({
                //         wireframe: true
                //     }));
                //     debugSphere.position.x = pos.x;
                //     debugSphere.position.y = pos.y;
                //     debugSphere.position.z = newZ;

                //     scene.add(debugSphere);
                //     objects.push(debugSphere);
                // }

                object.position.x = pos.x;
                object.position.y = pos.y;
                object.position.z = newZ;
                scene.add(object);
                objects.push(object);

            };
        },


        createShaderMaterial: function(id, uniforms) {

            var shader = THREE.ShaderTypes[id];

            var u = uniforms || THREE.UniformsUtils.clone(shader.uniforms);
            var vs = shader.vertexShader;
            var fs = shader.fragmentShader;

            var material = new THREE.ShaderMaterial({
                uniforms: u,
                vertexShader: vs,
                fragmentShader: fs
            });

            material.depthTest = true;
            material.side = THREE.DoubleSide;



            return material;

        },

        initThree: function(done) {


            renderer = new THREE.WebGLRenderer({
                // we need to preserve the drawing buffer when this generator is being used to output for print
                preserveDrawingBuffer: stuvio.env === 'print',
                antialias: true
            })

            camera = new THREE.PerspectiveCamera()
            camera.position.z = 1000;
            camera.far = CAMERAFAR;
            camera.near = CAMERANEAR;

            scene = new THREE.Scene()
            light = new THREE.DirectionalLight(0xff0000, 1);
            scene.add(light);


            // since the context is created by Three, we must add it to the Generator object
            // here since once initialize completes a context is required
            Generator.context = renderer.context;


            backgroundPlane = new THREE.Mesh(new THREE.PlaneBufferGeometry(BGWIDTH, BGHEIGHT), flatNoiseMaterial);

            done();

        },

        loadImages: function(i) {

            for (var i = 1; i < 8; i++) {
                this.loadImage("cutout" + i + ".png", cutouts, "cutouts");
            }

            for (var i = 1; i < 30; i++) {
                this.loadImage("brush" + i + ".png", brushes, "brushes");
            }


        },

        loadImage: function(name, array, folder) {

            var l = new THREE.ImageLoader(manager);

            l.load("assets/" + folder + "/" + name, function(image) {
                var t = new THREE.Texture(image);
                t.name = name;
                array.push(t);
            });

        },



        get3dPoint: function(x, y) {
            var vector = new THREE.Vector3();

            vector.set((x / WIDTH) * 2 - 1, -(y / HEIGHT) * 2 + 1, 0.5);

            vector.unproject(camera);

            var dir = vector.sub(camera.position).normalize();
            var distance = -camera.position.z / dir.z;
            return camera.position.clone().add(dir.multiplyScalar(distance));
        },

        loadFrames: function() {
            var objects = [
                "3Dframe1.obj",
                "3Dframe2.obj",
                "3Dframe3.obj",
                "3Dframe4.obj",
                "3Dframe5.obj",
                "3Dframe6.obj"
            ]
            var l = objects.length;
            var i = 0;

            for (i; i < l; i++) {
                this.loadObject(objects[i], frameGeometries, shinyMaterial);
            }
        },

        loadShapes: function() {


            for (var i = 0; i < phongObjectsUrl.length; i++) {
                this.loadObject(phongObjectsUrl[i], phongObjects);
            }

            for (var i = 0; i < otherObjectsUrl.length; i++) {
                this.loadObject(otherObjectsUrl[i], otherObjects);
            }


        },

        loadObject: function(s, array, material) {

            var loader = new THREE.OBJLoader(manager);

            loader.load('assets/objects/' + s, function(object) {

                var geometry;
                if (object.children.length) {
                    geometry = object.children[0].geometry;
                } else {
                    geometry = object.geometry;
                }

                // center geometry
                geometry.computeBoundingBox();
                var boundingBox = geometry.boundingBox;
                var vector = new THREE.Vector3().addVectors(geometry.boundingBox.max, geometry.boundingBox.min)
                vector.multiplyScalar(.5);
                geometry.applyMatrix(new THREE.Matrix4().makeTranslation(-vector.x, -vector.y, -vector.z));

                //  normalize dimensions
                geometry.computeBoundingSphere();
                var sphere = geometry.boundingSphere;
                var scale = 1 / (sphere.radius * 2);
                geometry.applyMatrix(new THREE.Matrix4().makeScale(scale, scale, scale));

                var mesh = new THREE.Mesh(geometry, material);
                mesh.name = s;

                array.push(mesh);

            }.bind(this), function() {}, function() {
                console.log("onerror")
            });

        },

        hexToNRgb: function(hex) {
            var bigint = parseInt(hex.substr(1), 16);
            return new THREE.Vector3(((bigint >> 16) & 255) / 255, ((bigint >> 8) & 255) / 255, (bigint & 255) / 255)
        },

        hslToRgb: function(h, s, l) {
            var r, g, b;

            if (s == 0) {
                r = g = b = l; // achromatic
            } else {
                var hue2rgb = function hue2rgb(p, q, t) {
                    if (t < 0) t += 1;
                    if (t > 1) t -= 1;
                    if (t < 1 / 6) return p + (q - p) * 6 * t;
                    if (t < 1 / 2) return q;
                    if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
                    return p;
                }

                var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
                var p = 2 * l - q;
                r = hue2rgb(p, q, h + 1 / 3);
                g = hue2rgb(p, q, h);
                b = hue2rgb(p, q, h - 1 / 3);
            }

            return new THREE.Vector3(r, g, b);
        },

        destroy: function(done) {

            done()
        }
    }

    return o;

})()