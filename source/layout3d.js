var Generator = (function() {

    var canvas = document.createElement('canvas')
    var context = canvas.getContext('2d')
    var MINRADIUS = 50;
    var width;
    var height;
    var scene;
    var camera;
    var renderer;
    var objects;
    var WIDTH = 520;
    var HEIGHT = 770;

    //margin between objects squared

    var points;

    var settings = {

        frameMargin: {
            type: 'number',
            label: 'frameMargin',
            range: [0, 100],
            value: 50,
            step: 1
        },
        seed: {
            type: 'number',
            label: 'Random seed',
            range: [0, 1000],
            value: 12,
            step: 1
        },
        centered: {
            type: 'boolean',
            label: 'Centered layout',
            value: true
        },

        maxRadius: {
            type: 'number',
            label: 'maxRadius',
            range: [10, 300],
            value: 100,
            step: 1
        },

        radiusOffset: {
            type: 'number',
            label: 'radiusOffset',
            range: [-100, 100],
            value: 0,
            step: 1
        },

    }

    return {

        context: context,

        settings: settings,

        initialize: function(done) {
            renderer = new THREE.WebGLRenderer({
                // we need to preserve the drawing buffer when this generator is being used to output for print
                preserveDrawingBuffer: stuvio.env === 'print',
                antialias: true
            })
            objects = [];
            camera = new THREE.PerspectiveCamera()
            camera.position.z = 1000;
            scene = new THREE.Scene();
            light = new THREE.DirectionalLight(0xffffff);

            scene.add(light)

            Generator.context = renderer.context;


            done();
        },

        generate: function(done) {
            width = this.context.canvas.width;
            height = this.context.canvas.height;

            stuvio.random.seed = settings.seed.value

            var frameMargin = settings.frameMargin.value;


            LayoutManager.go({
                radiusMin: MINRADIUS,
                radiusMax: settings.maxRadius.value + MINRADIUS,
                width: WIDTH - frameMargin * 2,
                height: HEIGHT - frameMargin * 2,
                startX: settings.centered.value ? WIDTH * .5 : stuvio.random.float(WIDTH),
                startY: settings.centered.value ? HEIGHT * .5 : stuvio.random.float(HEIGHT),
                onComplete: this.ok.bind(this)
            });

            done();

        },


        ok: function(points) {

            while (objects.length) {
                scene.remove(objects.pop());
            }

            camera.aspect = this.context.canvas.width / this.context.canvas.height
            camera.updateProjectionMatrix()
            renderer.setViewport(0, 0, this.context.canvas.width, this.context.canvas.height);
            var frameMargin = settings.frameMargin.value;


            for (var i = 0; i < points.length; i++) {

                var p = points[i];

                var pos = this.get3dPoint(p.x + frameMargin, p.y + frameMargin);
                var cube = new THREE.Mesh(new THREE.SphereGeometry(p.r), new THREE.MeshNormalMaterial());

                cube.rotation.x = stuvio.random.float(-Math.PI * 2, Math.PI * 2)
                cube.rotation.y = stuvio.random.float(-Math.PI * 2, Math.PI * 2)
                cube.rotation.z = stuvio.random.float(-Math.PI * 2, Math.PI * 2)
                cube.position.x = pos.x;
                cube.position.y = pos.y;

                objects.push(cube);
                scene.add(cube);
            }

            renderer.render(scene, camera)


        },

        get3dPoint: function(x, y) {
            var vector = new THREE.Vector3();

            vector.set((x / WIDTH) * 2 - 1, -(y / HEIGHT) * 2 + 1, 0.5);

            vector.unproject(camera);

            var dir = vector.sub(camera.position).normalize();
            var distance = -camera.position.z / dir.z;
            return camera.position.clone().add(dir.multiplyScalar(distance));
        },

        destroy: function(done) {

            done()
        }
    }

})();