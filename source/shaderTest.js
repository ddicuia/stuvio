var Generator = (function() {

    var CAMERAFAR = 6000;
    var CAMERANEAR = 0.1;
    var canvas = document.createElement('canvas')
    var context = canvas.getContext('2d')
    var width;
    var height;
    var scene;
    var manager = new THREE.LoadingManager();
    var camera;
    var renderer;
    var objectGeometries = [];
    var objects = [];
    var WIDTH = 520;
    var HEIGHT = 770;
    var BGWIDTH = Math.round(WIDTH * 1.25);
    var BGHEIGHT = Math.round(HEIGHT * 1.25);

    var backgroundPlane;


    var objectsURL = [
        "testcylinders.obj",
        "solid13.obj",
        "solid12.obj",
        "pipe5.obj",
        "pipe4.obj",
        "pipe3.obj",
        "pipe1.obj",
        "pipe2.obj",
        "smoothsolid5.obj",
        "polygonsolid3.obj",
        "smoothsolid4.obj",
        "polygonsolid2.obj",
        "solid10.obj",
        "solid9.obj",
        "solid11.obj",
        "plane6.obj",
        "plane4.obj",
        "plane5.obj",
        "plane3.obj",
        "plane2.obj",
        "plane1.obj",
        "loop3.obj",
        "smoothsolid3.obj",
        "polysphere.obj",
        "ellipsoid3.obj",
        "ellipsoid2.obj",
        "ellipsoid1.obj",
        "sphere.obj",
        "stick2.obj",
        "polystick2.obj",
        "stick1.obj",
        "polystick1.obj",
        "sausage7.obj",
        "sausage6.obj",
        "sausage5.obj",
        "sausage4.obj",
        "sausage3.obj",
        "sausage2.obj",
        "sausage1.obj",
        "loop2.obj",
        "loop1.obj",
        "smoothsolid2.obj",
        "trunc_cone4.obj",
        "trunc_cone3.obj",
        "trunc_cone2.obj",
        "trunc_cone1.obj",
        "solid8.obj",
        "solid7.obj",
        "polygonsolid1.obj",
        "smoothsolid1.obj",
        "rod2short.obj",
        "rod2mid.obj",
        "rod2.obj",
        "rod1short.obj",
        "rod1.obj",
        "solid5.obj",
        "solid6.obj",
        "solid4.obj",
        "solid3.obj",
        "solid2.obj",
        "solid1.obj",
        "column4.obj",
        "column3.obj",
        "column2.obj",
        "column1.obj"
    ];
    var noisyMaterial;
    var lambertMaterial;
    var chMaterial;
    var flatNoiseMaterial;
    var wfMaterial = new THREE.MeshBasicMaterial({
        wireframe: true
    });
    var light;
    var settings = {


        material: {
            type: 'select',
            label: 'Material',
            options: [{
                name: 'Noisy',
                value: 'noisy'
            }, {
                name: 'Wireframe',
                value: 'wireframe'
            }, {
                name: 'Shiny',
                value: 'shiny'
            }, {
                name: 'Matte',
                value: 'matte'
            }],
            display: 'name',
            value: {
                name: 'Noisy',
                value: 'noisy'
            }
        },

        lightColor: {
            type: 'color',
            label: 'Color',
            description: 'Example color input',
            value: '#4DECB4'
        },


        shininess: {
            type: 'number',
            label: 'shininess',
            range: [0.0, 100.0],
            value: 100,
            step: 1
        },


        lightDirection: {
            type: 'number',
            label: 'lightDirection',
            range: [0, 360],
            value: 0,
            step: 0.01
        },



        rotationX: {
            type: 'number',
            label: 'rotationX',
            range: [0, Math.PI * 2],
            value: 0,
            step: 0.01
        },

        rotationY: {
            type: 'number',
            label: 'rotationY',
            range: [0, Math.PI * 2],
            value: 0,
            step: 0.01
        },

        rotationZ: {
            type: 'number',
            label: 'rotationZ',
            range: [0, Math.PI * 2],
            value: 0,
            step: 0.01
        },



        objectIndex: {
            type: 'number',
            label: 'objectIndex',
            range: [0, objectsURL.length - 1],
            value: 0,
            step: 1
        }


    }



    return {

        context: context,

        settings: settings,

        initialize: function(done) {



            lambertMaterial = new THREE.MeshPhongMaterial({
                color: 0xffffff
            });

            lambertMaterial.side = THREE.DoubleSide;

            renderer = new THREE.WebGLRenderer({
                // we need to preserve the drawing buffer when this generator is being used to output for print
                preserveDrawingBuffer: stuvio.env === 'print',
                antialias: true
            })
            camera = new THREE.PerspectiveCamera()
            camera.position.z = 1000;
            camera.position.z = 2000;
            camera.far = CAMERAFAR;
            camera.near = CAMERANEAR;
            scene = new THREE.Scene();

            Generator.context = renderer.context;

            light = new THREE.DirectionalLight(0xffffff);
            noisyMaterial = this.createShaderMaterial("noisyDiffuse");
            flatNoiseMaterial = this.createShaderMaterial("flatNoise");

            scene.add(light)

            var planeGeometry = new THREE.PlaneGeometry(BGWIDTH, BGHEIGHT);
            backgroundPlane = new THREE.Mesh(planeGeometry, flatNoiseMaterial);
            // backgroundPlane.rotation.x = -.2;
            scene.add(backgroundPlane);


            manager.onLoad = function(item, loaded, total) {
                console.log("all loaded");

                // sort arrays alphabetically to ensure same results 
                objectGeometries.sort(alphaSort);

                function alphaSort(a, b) {
                    if (a.name < b.name) return 1;
                    if (a.name > b.name) return -1;
                    return 0;
                }

                done();
            }.bind(this);
            this.loadObjects();



        },

        hexToNRgb: function(hex) {
            var bigint = parseInt(hex.substr(1), 16);
            return new THREE.Vector3(((bigint >> 16) & 255) / 255, ((bigint >> 8) & 255) / 255, (bigint & 255) / 255)
        },

        generate: function(done) {
            width = this.context.canvas.width;
            height = this.context.canvas.height;
            camera.aspect = width / height
            camera.updateProjectionMatrix()
            renderer.setClearColor(0xdddddd);
            renderer.setViewport(0, 0, width, height);


            while (objects.length > 0) {
                scene.remove(objects[0]);
                objects.pop();
            }

            console.log(scene.children)
            var color = this.hexToNRgb(settings.lightColor.value);
            light.color.setHex(parseInt(settings.lightColor.value.substr(1).toUpperCase(), 16));


            var newZ = -600;
            var newWidth = (width * 1.25) * -((newZ - camera.position.z) / camera.position.z);
            var newScale = (newWidth / BGWIDTH)

            backgroundPlane.scale.x = newScale;
            backgroundPlane.scale.y = newScale;
            backgroundPlane.position.z = newZ;


            cube = objectGeometries[settings.objectIndex.value];
            objects.push(cube);

            switch (settings.material.value.value) {
                case 'noisy':
                    cube.material = noisyMaterial;
                    break;
                case 'wireframe':
                    cube.material = wfMaterial;
                    break;
                case 'shiny':
                    cube.material.shininess = settings.shininess.value;
                    cube.material = lambertMaterial;
                    break;
                case 'matte':
                    cube.material.shininess = 0;
                    cube.material = lambertMaterial;
                    break;


            }

            cube.rotation.z = settings.rotationZ.value;
            cube.rotation.y = settings.rotationY.value;
            cube.rotation.x = settings.rotationX.value;

            var theta = settings.lightDirection.value / 180 * Math.PI - Math.PI;
            light.position.x = Math.cos(theta);
            light.position.y = Math.sin(theta);
            light.position.z = 0.5;

            flatNoiseMaterial.uniforms.gradientColor.value = color;
            noisyMaterial.uniforms.color.value = color;
            noisyMaterial.uniforms.uDirLightPos.value = light.position;
            noisyMaterial.uniforms.uKd.value = 0.3;

            scene.add(cube);



            renderer.render(scene, camera)
            done();
        },


        loadObjects: function() {


            var l = 1;
            var i = 0;
            for (var i = 0; i < objectsURL.length; i++) {
                this.loadObject(objectsURL[i], objectGeometries, noisyMaterial);
            }
        },

        loadObject: function(s, array, material) {

            var loader = new THREE.OBJLoader(manager);

            loader.load('assets/objects/' + s, function(object) {

                var geometry;
                geometry = object.children[0].geometry;

                // center geometry
                geometry.computeBoundingBox();
                var boundingBox = geometry.boundingBox;
                var vector = new THREE.Vector3().addVectors(geometry.boundingBox.max, geometry.boundingBox.min)
                vector.multiplyScalar(.5);
                geometry.applyMatrix(new THREE.Matrix4().makeTranslation(-vector.x, -vector.y, -vector.z));

                //  normalize dimensions
                geometry.computeBoundingSphere();
                var sphere = geometry.boundingSphere;
                var scale = 1 / (sphere.radius * 2);
                geometry.applyMatrix(new THREE.Matrix4().makeScale(scale, scale, scale));

                var m = new THREE.Mesh(geometry, material);
                m.scale.multiplyScalar(1000);
                m.name = s;

                array.push(m);

            }.bind(this), function() {}, function() {
                console.log("onerror")
            });

        },

        createShaderMaterial: function(id, uniforms) {

            var shader = THREE.ShaderTypes[id];

            var u = uniforms || THREE.UniformsUtils.clone(shader.uniforms);
            var vs = shader.vertexShader;
            var fs = shader.fragmentShader;

            var material = new THREE.ShaderMaterial({
                uniforms: u,
                vertexShader: vs,
                fragmentShader: fs
            });


            material.side = THREE.DoubleSide;
            material.depthTest = true;

            return material;
        },



        destroy: function(done) {

            done()
        }
    }

})();