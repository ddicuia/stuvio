var Generator = (function() {

    var canvas = document.createElement('canvas')
    var context = canvas.getContext('2d')

    var settings = {

        backgroundColor: {
            type: 'color',
            label: 'Background Color',
            value: '#d9ebee'
        }
    }
    var poisson;

    return {

        context: context,

        settings: settings,

        initialize: function(done) {

            done()
        },

        generate: function(done) {
            var width = canvas.width;
            var height = canvas.height;
            poisson = new PoissonDiscDistribution({
                start: {
                    x: width / 2,
                    y: height / 2,
                },
                width: width,
                height: height,
            });

            for (var i = 0; i < 800; i++) {
                poisson.next();
            }

            context.fillStyle = settings.backgroundColor.value
            context.fillRect(0, 0, canvas.width, canvas.height)

            for (var i = 0; i < poisson.points.length; i++) {

                var p = poisson.points[i];
                context.beginPath();
                context.fillStyle = "hsl(" + (stuvio.random.int(360)) + ", 72%, 63%)";
                context.arc(p.x, p.y, p.radius, 0, 2 * Math.PI, false);
                context.fill();

            }

            done()
        },

        destroy: function(done) {

            done()
        }
    }

})();